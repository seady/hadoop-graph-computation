package com.graph;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.util.*;

public class GraphDriver extends Configured implements Tool {

	public int run(String[] args) throws Exception {

		String hadoopConfDir = System.getenv("HADOOP_HOME") + "/etc/hadoop/";

		Configuration conf = this.getConf();
		conf.addResource(new Path(hadoopConfDir + "mapred-site.xml"));
		conf.addResource(new Path(hadoopConfDir + "yarn-site.xml"));
		conf.addResource(new Path(hadoopConfDir + "hdfs-site.xml"));
		conf.addResource(new Path(hadoopConfDir + "core-site.xml"));

		// set the K value in job configuration Object, we will read this value
		// in reducer
		conf.setInt("K", Integer.parseInt(args[2]));
		
		FileSystem fileSystem = FileSystem.get(getConf());

		Path outputDir = new Path(args[1]);

		if (fileSystem.exists(outputDir)) {
			fileSystem.delete(outputDir, true);
		}

		Job graphJob = Job.getInstance(conf,"Graph Computation");
		
		graphJob.setJarByClass(com.graph.GraphDriver.class);
		
		graphJob.setMapperClass(com.graph.GraphMap.class);
		graphJob.setReducerClass(com.graph.GraphReducer.class);
		graphJob.setCombinerClass(com.graph.GraphCombiner.class);

		graphJob.setMapOutputKeyClass(Text.class);
		graphJob.setMapOutputValueClass(IntWritable.class);
		graphJob.setOutputKeyClass(Text.class);
		graphJob.setOutputValueClass(IntWritable.class);
		graphJob.setNumReduceTasks(1);

		FileInputFormat.addInputPath(graphJob, new Path(args[0]));

		FileOutputFormat.setOutputPath(graphJob, new Path(args[1]));

		// Execute graphJob and return status
        return graphJob.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		
		long start = new Date().getTime();
		int res = -1;
		try {
			if (args.length != 3) {
				System.out.println("Please provide proper argument..");
				System.out.println("InputDirectory OutputDirectory K-Value");
				return;
			}
			res = ToolRunner.run(new Configuration(), new GraphDriver(), args);
			
			long end = new Date().getTime();
			System.out.println("Graph Job Took " + (end - start) / 60000
					+ " minutes.");
			System.exit(res);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(res);
		}
	}
}
