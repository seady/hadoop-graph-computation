package com.graph;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class GraphCombiner extends Reducer<Text,IntWritable,Text,IntWritable>{

	int edgesCount = 0;
	int verticesCount = 0;
	
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

    }

	@Override
	protected void reduce(Text key, Iterable<IntWritable> values,
			Context context) throws IOException, InterruptedException {
		int degreeCount = 0;

		// if the key contains Vertex identifier
		//count the number of degrees in the combiner
		if (key.toString().contains("Vertex")) {
			
			// count the number of degrees for current vertex
			for (IntWritable value : values) {
				degreeCount += value.get();
			}
			
        	context.write(key, new IntWritable(degreeCount));
		}
		
		if (key.toString().contains("Edge")) {
			//pass on the edge to the reducer
			context.write(key, new IntWritable(1));
		}
	}

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {

    }

}
