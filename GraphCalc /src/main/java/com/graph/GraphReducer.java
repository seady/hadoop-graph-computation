package com.graph;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class GraphReducer extends Reducer<Text, IntWritable, Text, Text> {

	int verticesCount = 0;
	int edgesCount = 0;
	TreeMap<Integer, Integer> verticesWithDegrees = new TreeMap<Integer, Integer>();
	
	Map<Integer, Integer> topKdegrees = new HashMap<Integer, Integer>();

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {

	}

	@Override
	protected void reduce(Text key, Iterable<IntWritable> values,
			Context context) throws IOException, InterruptedException {

		int degreeCount = 0;

		// if the key contains Vertex identifier
		if (key.toString().contains("Vertex")) {

			// count the number of unique vertex by incrementing the vertex
			// count
			verticesCount += 1;

			// count the number of degrees for current vertex
			for (IntWritable value : values) {
				degreeCount += value.get();
			}

			// add the current vertex to the vertex and their degree collection
			// TreeMap<Integer, String> will sorted based on key, so add the
			// degree as key
			verticesWithDegrees.put(Integer.parseInt(key.toString().split(" ")[1]), degreeCount);
		}

		// if the key contains Edge identifier
		if (key.toString().contains("Edge")) {
			// increment the vertex count
			edgesCount += 1;
		}

	}

	@Override
	protected void cleanup(Context context) throws IOException,
			InterruptedException {

		String vertex,degree;
		
		context.write(new Text("Number of Vertices: "), new Text(String.valueOf(verticesCount)));

		context.write(new Text("Number of Edges: "),new Text(String.valueOf(edgesCount)));

		//Just for customization
		context.write(new Text(), new Text());
		
		context.write(new Text("All Vertices with Degree.... "), new Text());
		context.write(new Text("Vertex\tDegree "), new Text());

		Set<?> set = verticesWithDegrees.entrySet();
		Iterator<?> iterator1 = set.iterator();

		// /write all the vertices with their number of degree count
		while (iterator1.hasNext()) {

			Map.Entry mentry = (Map.Entry) iterator1.next();			
			vertex = mentry.getKey().toString();
			degree = mentry.getValue().toString();
			context.write(new Text(vertex), new Text(degree));
		}
		//Just for customization
		context.write(new Text(), new Text());
		
		// /write the k vertices with highest number of degree count
		context.write(new Text("Top K Vertices With Degree...."), new Text());
		context.write(new Text("Vertex\tDegree "), new Text());
		
		//read the K value from configuration object
		int k = context.getConfiguration().getInt("K", 1);
		int itemCount = 1;

		topKdegrees = entriesSortedByValues(verticesWithDegrees);
		set = topKdegrees.entrySet();
		Iterator<?> iterator2 = set.iterator();
		
		while (iterator2.hasNext()) {
			if (itemCount <= k) {
				Map.Entry mentry = (Map.Entry) iterator2.next();
				vertex = mentry.getKey().toString();
				degree = mentry.getValue().toString();
				context.write(new Text(vertex), new Text(degree));
				itemCount++;
			}
			else{
				break;
			}
		}
	}

	//sort the vertices based on the degrees in descending order
	public static <K, V extends Comparable<? super V>> Map<K, V> entriesSortedByValues(
			Map<K, V> map) {
		
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
				map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
}
