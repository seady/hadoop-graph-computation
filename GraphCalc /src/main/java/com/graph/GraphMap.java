package com.graph;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import java.io.IOException;

public class GraphMap extends Mapper<LongWritable,Text,Text,IntWritable> {


	String[] vertices= new String[2];
	Text eachVertex = new Text();
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    	
    	///Split each edge into vertices
    	vertices = value.toString().split("\t");
    	
    	if(value.toString().split("\t").length == 2)
    	{
    		/////////Start writing the vertices to map output///////////

    		//clear the variable before setting a new value
        	eachVertex.clear();
        	//Set the identifier Vertex to identify in reducer/combiner
        	eachVertex.set("Vertex " + vertices[0].trim());
        	context.write(eachVertex, new IntWritable(1));
        	
        	//clear the variable before setting a new value
        	eachVertex.clear();
        	//Set the identifier Vertex to identify in reducer/combiner
        	eachVertex.set("Vertex " + vertices[1].trim());
        	context.write(eachVertex, new IntWritable(0));
        	
        	/////////End writing the vertices to map output ///////////
    	}
    	
    	//Set the identifier to identify in reducer/combiner
    	value.set("Edge " + value.toString());
    	
    	/////Write the each edge to the map output
        context.write(value, new IntWritable(1));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {

    }

}
