# Mini projet Hadoop

**Nom:** Semigambi

**Prénom:** Andy-Saleh

**Groupe:** SSSR

*NOTE: Veuillez ajouter la ligne suivante dans le fichier **mapred-site.xml** qui se trouve dans le répertoire **/etc/hadoop/conf/**  afin d'allouer plus de la mémoire dans le HDFS:*

```
  <property>
    <name>mapred.child.java.opts</name>
    <value>-Xmx4096m</value>
  </property>
```

*si non,l'exécution renvoyera toujours Status Failed(sur machine virtuelle des salles TP).*


**Input Dataset:** "California road network"

 [http://snap.stanford.edu/data/roadNet-CA.html]([http://snap.stanford.edu/data/roadNet-CA.html/)  
 
 **git Repository:**
 
 https://seady@bitbucket.org/seady/hadoop-graph-computation.git
[bitbucket](https://seady@bitbucket.org/seady/hadoop-graph-computation.git)


##1.Calcul du nombre de sommets dans un graphe

#### Mapper 
* input:<Key:VertexId (décalage de la ligne depuis le début du fichier), Value : lignes du fichier
* output:<VertexID, occurence de chaque vertex>
* setup->Map
* Map->cleanup

#### Reducer
* input:<VertexID, occurence de chaque vertex>
* output:<"Number of Vertices:", verticesCount>
* setup->Reduce
* Reduce->cleanup(<"Number of Vertices:", verticesCount>) 

#### Combiner
* input:<VertexID, occurence de chaque vertex>
* output:<"Number of Vertices:", verticesCount>
* setup->Combine
* Combine->cleanup


##2.Calcul du nombre d'arêtes dans un graphe

#### Mapper 
* input:<Key: EdgeID, Value>
* output:<EdgeID, chaque degré occurente>
* setup->Map
* Map->cleanup

#### Reducer
* input:Key :<EdgeID, occurence>
* output:<"Number of Edges:",Value: edgesCount>
* setup->Reduce
* Reduce->cleanup(<"Number of Edges:", edgesCount>) 

#### Combiner
* input:EdgeID
* output:<"Number of Edges:", edgesCount>
* setup->Reduce
* Reduce->cleanup 


##3.Calcul du degré de chaque sommet. Le degré d'un sommet est le nombre de ces voisins.

### Mapper 
* input:<Key:VertexId (décalage de la ligne depuis le début du fichier), Value : lignes du fichier
* output:<vertexID, occurence de chaque vertice >
* setup->Map
* Map->cleanup

#### Reducer
* input:<vertexID, TreeMap Of vertexDegree>
* output:<vertex, degree>
* setup->Reduce
* Reduce->cleanup(<vertex, degree>) 

#### Combiner
* input:<Collection(TreeMap) de vertexDegree>
* output:<vertex, degree>
* setup->Reduce
* Reduce->cleanup

##4.Le Top-K des sommets ayant le plus grand degrés.

### Mapper 
* input:<Key:VertexId (décalage de la ligne depuis le début du fichier), Value : lignes du fichier
* output:<vertexID, occurence de chaque vertice >
* setup->Map
* Map->cleanup

#### Reducer
* input:Collection(TreeMap) de vertexDegree triée dans l'ordre décroissant >
* output:<vertex, degree>
* setup->Reduce
* Reduce->cleanup(<vertex, degree>) 

#### Combiner
* input:Collection(TreeMap) de vertexDegree triée dans l'ordre décroissant >
* output:<vertex, degree>
* setup->Reduce
* Reduce->cleanup

**Notes:**Le code est bien commenté.

###Je mettrais du code avec Scala dans les jours à venir


##Exemple pour mettre un bloc Flink : 

```
package com.flink.graph.graphCalc;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;

public class PointAssigner
implements JoinFunction<Tuple2<Tuple2<Long, Long>, Long>, Tuple2<Tuple2<Long, Long>, Long>, Tuple2<Tuple2<Tuple2<Long, Long>, Long>, Tuple2<Tuple2<Long, Long>, Long>>> {

/**
* 
*/
private static final long serialVersionUID = 1L;

@Override
public Tuple2<Tuple2<Tuple2<Long, Long>, Long>, Tuple2<Tuple2<Long, Long>, Long>> join(Tuple2<Tuple2<Long, Long>, Long> jdd1, Tuple2<Tuple2<Long, Long>, Long> jdd2) {

	//Tuple2<Tuple2<Long, Long>, Long> empty = new Tuple2<Tuple2<Long, Long>, Long> ();
return new Tuple2<Tuple2<Tuple2<Long, Long>, Long>, Tuple2<Tuple2<Long, Long>, Long>> (jdd1, (jdd2 != null)  ? jdd2 : null);
}
}

```

*A venir avec Flink*

###Lancement
```
mvn clean package
hadoop jar GraphCalc.jar GraphDriver input output k-value


   


